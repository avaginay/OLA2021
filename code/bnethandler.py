import PyBoolNet


def bnetfile2igdot(bnetfilepath, igdotpath, styles=["sccs", "anonymous"]):
    """
    Arguments :
    -----------
    - bnetfilepath (str) path to the bnet file
    - igdotpath (str) path to where the ig graph will be stored (dot format)
    - style (array)
    """
    # Load the bnet file :
    primes = PyBoolNet.FileExchange.bnet2primes(bnetfilepath)

    # Plot the interaction graph
    # use predifined style : (scc : strongly connected component)
    PyBoolNet.InteractionGraphs.create_image(
        primes, igdotpath, Styles=styles)


def bn_stats(primes):

    # number of nodes and their name
    print(len(primes))
    print(primes.keys())

    # nb of constant nodes and their name
    const = PyBoolNet.PrimeImplicants.find_constants(primes)
    print(len(const))
    print(const)

    # nb of configurations :
    print(2**len(primes))
