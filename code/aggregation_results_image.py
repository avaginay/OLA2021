
import argparse
import pathlib
import textwrap

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import seaborn as sns


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Produce a table giving the number of BNs generated for all systems by all teh identification methods',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--aggregation_results_table',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                            Path to the file which aggregate the results
                            ''')
                        )
    parser.add_argument('--pattern',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        The pattern that will be greped in the column 'system' of the aggragation table.
                        All the lines which have this pattern will be used to plot the figure.
                        ''')
                        )

    parser.add_argument('--jitter_x',
                        type=float,
                        required=True,
                        help=textwrap.dedent('''\
                        Jitter to apply on the x axis
                        ''')
                        )
    parser.add_argument('--jitter_y',
                        type=float,
                        required=True,
                        help=textwrap.dedent('''\
                        Jitter to apply on the y axis
                        ''')
                        )
    parser.add_argument('--hists',
                        type=str2bool,
                        required=True,
                        help=textwrap.dedent('''\
                        Whether adding the histograms around the scatter plots
                        ''')
                        )
    parser.add_argument('--pathout',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to where the aggegation figure for the fiven pattern
                        ''')
                        )

    # Fixing random state for reproducibility
    np.random.seed(42)

    args = parser.parse_args()
    # sys.argv = []
    # args = parser.parse_args(sys.argv)

    print(args)
    print(args.pattern)

    aggregation_table = pd.read_csv(
        args.aggregation_results_table, index_col=0)

    considered_rows = aggregation_table[
        aggregation_table.index.str.contains(args.pattern)]

    # scatter plot with histograms :
    # https://matplotlib.org/3.2.1/gallery/lines_bars_and_markers/scatter_hist.html

    # define figure dimention
    fig_width = 8
    fig_height = 6
    # , constrained_layout=True)
    fig = plt.figure(figsize=(fig_width, fig_height))
    # fig = plt.figure()

    fig.tight_layout()

    if args.hists:
        # Add a gridspec with two rows and two columns and a ratio of 2 to 7 between
        # the size of the marginal axes and the main axes in both directions.
        # Also adjust the subplot parameters for a square plot.
        gs = fig.add_gridspec(2, 2,
                              width_ratios=(2, 6), height_ratios=(3, 6),
                              # késoko :
                              left=0, right=0.9, bottom=0, top=0.9,
                              wspace=0.1, hspace=0.1
                              )

        ax_scatter = fig.add_subplot(gs[1, 1])

        ax_histx = fig.add_subplot(gs[0, 1], sharex=ax_scatter)
        ax_histx.set_xlabel("median", fontweight="bold")
        ax_histx.set_ylabel("     Probability density")
        plt.setp(ax_histx.get_xticklabels(), visible=False)
        ax_histx.xaxis.set_label_position('top')
        ax_histx.xaxis.set_ticks_position('bottom')
        ax_histx.yaxis.set_label_position('left')
        ax_histx.yaxis.set_ticks_position('left')
        # Make the tik label invisible
        plt.setp(ax_histx.get_xticklabels(), visible=False)
        # ax_histx.tick_params(axis='x', colors='white')

        ax_histy = fig.add_subplot(gs[1, 0], sharey=ax_scatter)
        # labels are inverted since the plot will be in oposite direction
        ax_histy.set_xlabel("Probability density     ")
        ax_histy.set_ylabel("std", fontweight="bold")
        ax_histy.xaxis.set_label_position('top')
        ax_histy.xaxis.set_ticks_position('top')
        ax_histy.yaxis.set_label_position('left')
        ax_histy.yaxis.set_ticks_position('right')
        ax_histy.tick_params(axis='y', labelrotation=90)
        plt.setp(ax_histy.get_yticklabels(), visible=False)
        # empty_string_labels = ['']*len(labels)
        # ax_histy.set_yticklabels([''])
        # ax_histy.tick_params(
        #     axis='both',
        #     # which='major',
        #     # labelsize=10,
        #     labelbottom=False,
        #     bottom=True,
        #     top=True,
        #     labeltop=False
        # )

        # no labels
        # ax_histx.tick_params(axis="x", labelbottom=False)
        # ax_histy.tick_params(axis="y", labelleft=False)

    else:
        ax_scatter = fig.add_subplot(111)
        # 111 = "1x1 grid, first subplot"

    title = fig.suptitle(args.pattern)
    # ax_scatter.set_title(args.pattern)

    # ax_scatter.set_xlabel('median')
    # ax_scatter.xaxis.set_label_position('top')
    ax_scatter.xaxis.set_ticks_position('top')
    ax_scatter.set_xlim(-0.07, 1.07)

    # Test to put away the points concerning xp which returned no BNs at all:
    # ax_scatter.set_xlim(-0.6, 1.1)
    #
    # # the - 1 is for method having return no BNs
    # def update_ticks(x, pos):
    #     if x == -0.5:
    #         return 'no BNs'
    #     if x < 0:
    #         return ''
    #     else:
    #         return x
    # ax_scatter.xaxis.set_major_formatter(mticker.FuncFormatter(update_ticks))

    ax_scatter.yaxis.set_ticks_position('left')
    ax_scatter.set_ylim(0.45, -0.07, )
    ax_scatter.tick_params(axis='y', labelrotation=90)

    # Hide the right and bottom spines
    # ax_scatter.spines['right'].set_visible(False)
    # ax_scatter.spines['bottom'].set_visible(False)

    for identification_method, color, shape in zip(
        ["reveal", "bestfit", "caspots", "our"],
        ["black", "darkgreen", "darkblue", "red"],
        ["X", "s", "v", "o"]
    ):

        # x = _propcov_median
        x_coord = considered_rows[identification_method +
                                  "_propcov_median"]

        # y = _propcov_std
        y_coord = considered_rows[identification_method + "_propcov_std"]

        # choose what to do if no BN return was return
        #   - put 0
        #   - put np.nan (to not draw them...)
        #   - median is -0.5 (to put it away) :
        if identification_method in ["reveal", "bestfit"]:
            suffix_nbBNs = "_nbBNs_withInfluenceSignsOK"
        else:
            suffix_nbBNs = ""
        x_coord.loc[
            considered_rows[identification_method + suffix_nbBNs] == 0] = 0
        y_coord.loc[
            considered_rows[identification_method + suffix_nbBNs] == 0] = 0

        jittered_x = x_coord + args.jitter_x * \
            np.random.rand(len(x_coord)) - (args.jitter_x / 2)
        jittered_y = y_coord + args.jitter_y * \
            np.random.rand(len(y_coord)) - (args.jitter_y / 2)

        print(identification_method)
        print(list(zip(jittered_x, jittered_y)))

        labels_fix = {
            "reveal": "REVEAL",
            "bestfit": "Best-Fit",
            "caspots": "caspots",
            "our": "ours"
        }

        ax_scatter.scatter(jittered_x, jittered_y,
                           s=40,
                           # c = color,
                           facecolor="none",  # fill color
                           edgecolor=color,  # edge of the shape
                           lw=0.7,  # linewidth
                           marker=shape,
                           label=labels_fix[identification_method],
                           alpha=0.8  # 0.2
                           )

        # if args.hists:
        #     # https://towardsdatascience.com/histograms-and-density-plots-in-python-f6bda88f5ac0
        #     # Density Plot with Rug Plot
        #     sns.distplot(
        #         x_coord,
        #         ax=ax_histx,
        #         norm_hist=True,
        #         hist=False,
        #         kde=True,
        #         rug=False,
        #         color=color,
        #         kde_kws={'linewidth': 1},
        #         rug_kws={'color': color}
        #     )
        #     sns.distplot(
        #         y_coord,
        #         ax=ax_histy,
        #         norm_hist=True,
        #         hist=False,
        #         kde=True,
        #         rug=False,
        #         color=color,
        #         kde_kws={'linewidth': 1},
        #         rug_kws={'color': color},
        #         vertical=True
        #     )

        if args.hists:
            density_x = sns.kdeplot(
                x_coord,
                ax=ax_histx,
                # bw = 0.5 , # Smoothing parameter.. deprecated
                color=color,
                linewidth=1,
                label=""
            )

            density_y = sns.kdeplot(
                y_coord,
                ax=ax_histy,
                # bw = 0.5 , # Smoothing parameter.. deprecated
                color=color,
                linewidth=1,
                vertical=True,
                label=""
            )

        # if args.hists:
        #     import scipy.stats
        #
        #     x_coord_density =  scipy.stats.gaussian_kde(x_coord)
        #

        # if args.hists:
        #
        #     # determine nice limits by hand:
        #     #binwidth = 0.25
        #     #xymax = max(np.max(np.abs(x_coord)), np.max(np.abs(y_coord)))
        #     #lim = (int(xymax / binwidth) + 1) * binwidth
        #
        #     #bins = np.arange(-lim, lim + binwidth, binwidth)
        #     ax_histx.hist(
        #         x_coord,
        #         density=False,
        #         # About density :
        #         # If True, the result is the value of the probability density function at the bin,
        #         # normalized such that the integral over the range is 1.
        #         # Note that the sum of the histogram values will not be equal to 1 unless bins of unity width are chosen;
        #         # it is not a probability mass function.
        #         # ---
        #         # https://github.com/matplotlib/matplotlib/issues/10398/#issuecomment-366021979
        #         # for the bars to sum to one (independent of the bin widths),
        #         # given density = False and weigths = np.ones(len(y_coord)) / len(y_coord)
        #         weights=np.ones(len(x_coord)) / len(x_coord),
        #         histtype='step',
        #         # bins=bins,
        #         color=color,
        #         edgecolor=color,
        #         fill=False,
        #         alpha=0.8
        #     )
        #     ax_histy.hist(
        #         y_coord,
        #         density=False,
        #         weights=np.ones(len(y_coord)) / len(y_coord),
        #         histtype='step',
        #         # bins=bins,
        #         color=color,
        #         edgecolor=color,
        #         fill=False,
        #         alpha=0.8,
        #         orientation='horizontal'
        #     )

    # legend outside the figure :
    # lgd = plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
    # lgd = plt.legend(bbox_to_anchor=(7, 7), loc='upper left')
    # lgd = ax_scatter.legend(bbox_to_anchor=(1.05, -0.05), bbox_transform=ax_scatter.transAxes,
    #                         loc='upper left')
    lgd = ax_scatter.legend(bbox_to_anchor=(-0.40, 0.9), bbox_transform=ax_histx.transAxes,
                            loc='upper left')

    # adjust the subplot params when it is called
    # plt.tight_layout()

    # plt.show()

    ax_histy.invert_xaxis()

    # be carefull that th efigure box does not cut the legend :
    # https://stackoverflow.com/q/10101700
    fig.savefig(
        args.pathout,
        bbox_extra_artists=(
            title, lgd,),
        bbox_inches='tight'
    )

    print("the end")
