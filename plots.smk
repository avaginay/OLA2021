import pathlib

include: "BN-analysis.smk"


for key in (
        "folder_pkn",
        "folder_ts",
        "folder_bn"
):
    print("->" + key)
    print(config[key])
    config[key] = pathlib.Path(config[key])

gW_SYSTEM_id_ts, = glob_wildcards(
    config['folder_ts'] / "{SYSTEM_id}_ts.csv")
gW_SYSTEM_id_pkn, = glob_wildcards(
    config['folder_pkn'] / "{SYSTEM_id}_pkn.sif")
gW_SYSTEM_id = gW_SYSTEM_id_ts + gW_SYSTEM_id_pkn

gW_SYSTEM_id_generatedbnet, gW_identification_method, gW_BNid_generatedbnet = glob_wildcards(
    config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" / "{BN_id}.bnet")
print("generated bnet:")
print(gW_SYSTEM_id_generatedbnet)
print(gW_identification_method)
print(gW_BNid_generatedbnet)


rule all_plots:
    input:
        # ts plots
        expand(config['folder_ts'] /
               "{SYSTEM_id}_tsraw.png", SYSTEM_id=gW_SYSTEM_id),
        expand(config['folder_ts'] /
               "{SYSTEM_id}_tsstretched.png", SYSTEM_id=gW_SYSTEM_id),
        expand(config['folder_ts'] /
               "{SYSTEM_id}_histtsstretched.png", SYSTEM_id=gW_SYSTEM_id),
        expand(config['folder_ts'] /
               "{SYSTEM_id}_tsbinarized.png", SYSTEM_id=gW_SYSTEM_id),
        # interaction graph
        expand(config['folder_pkn'] /
               "{SYSTEM_id}.svg", SYSTEM_id=gW_SYSTEM_id),
        # dynamic(config['folder_pkn'] / "{SYSTEM_id}_{node}.svg")
        # stg
        expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
               "{BN_id}_stg-synchronous_complete.png",
               zip,
               SYSTEM_id=gW_SYSTEM_id_generatedbnet,
               identification_method=gW_identification_method,
               BN_id=gW_BNid_generatedbnet
               ),
        # do not put thing one, can be extra big :
        # expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}"/
        #       "{BN_id}_stg-asynchronous_complete.png", zip, SYSTEM_id=gW_SYSTEM_id_generatedbnet, identification_method = gw_identification_method, BN_id=gW_BNid_generatedbnet),
        expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
               "{BN_id}_stg-synchronous_focusedbts.png",
               zip,
               SYSTEM_id=gW_SYSTEM_id_generatedbnet,
               identification_method=gW_identification_method,
               BN_id=gW_BNid_generatedbnet
               ),
        expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
               "{BN_id}_stg-asynchronous_focusedbts.png",
               zip,
               SYSTEM_id=gW_SYSTEM_id_generatedbnet,
               identification_method=gW_identification_method,
               BN_id=gW_BNid_generatedbnet
               )


rule all_tsplots:
    input:
        # ts plots
        expand(config['folder_ts'] /
               "{SYSTEM_id}_tsraw.png", SYSTEM_id=gW_SYSTEM_id),
        expand(config['folder_ts'] /
               "{SYSTEM_id}_tsstretched.png", SYSTEM_id=gW_SYSTEM_id),
        expand(config['folder_ts'] /
               "{SYSTEM_id}_histtsstretched.png", SYSTEM_id=gW_SYSTEM_id),
        expand(config['folder_ts'] /
               "{SYSTEM_id}_tsbinarized.png", SYSTEM_id=gW_SYSTEM_id),


rule all_stgpng:
    input:
        expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
               "{BN_id}_stg-synchronous_complete.png",
               zip,
               SYSTEM_id=gW_SYSTEM_id_generatedbnet,
               identification_method=gW_identification_method,
               BN_id=gW_BNid_generatedbnet
               ),
        # /!\ can be extra big !!:
        expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
               "{BN_id}_stg-asynchronous_complete.png",
               zip,
               SYSTEM_id=gW_SYSTEM_id_generatedbnet,
               identification_method=gW_identification_method,
               BN_id=gW_BNid_generatedbnet
               ),
        expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
               "{BN_id}_stg-synchronous_focusedbts.png",
               zip,
               SYSTEM_id=gW_SYSTEM_id_generatedbnet,
               identification_method=gW_identification_method,
               BN_id=gW_BNid_generatedbnet
               ),
        expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
               "{BN_id}_stg-asynchronous_focusedbts.png",
               zip,
               SYSTEM_id=gW_SYSTEM_id_generatedbnet,
               identification_method=gW_identification_method,
               BN_id=gW_BNid_generatedbnet
               )


rule all_coverageboxplot:
    input:
        expand(
            config["folder_bn"] / "{SYSTEM_id}" /
            "{identification_method}" /
            "coverage-{SYSTEM_id}-{identification_method}-retreived_edges__number-mixed-boxplot_stgVSts.png",
            SYSTEM_id=set(gW_SYSTEM_id_generatedbnet),
            identification_method=set(gW_identification_method)
        )
print("##################################")
print(gW_SYSTEM_id_generatedbnet)
print("##################################")

#
# rule all_coverageboxplot:
#     input:
#         expand(
#             config["folder_bn"] / "{SYSTEM_id}" /
#             "{identification_method}" /
#             "coverage-{SYSTEM_id}-{identification_method}-{measure}-{updatescheme}-boxplot_stgVSts.png",
#             SYSTEM_id=set(gW_SYSTEM_id_generatedbnet),
#             identification_method=set(gW_identification_method),
#             measure=["retreived_edges__number", "retreived_edges__proportion"],
#             updatescheme=["asynchronous"]
#         )

rule ts2plots:
    input:
        ts = config['folder_ts'] / "{SYSTEM_id}_ts.csv"
    params:
        outdir = config['folder_ts'],
        prefix = "{SYSTEM_id}_"
    output:
        [config['folder_ts'] / filename for filename in [
            "{SYSTEM_id}_tsraw.png",
            "{SYSTEM_id}_tsstretched.png",
            "{SYSTEM_id}_histtsstretched.png",
            "{SYSTEM_id}_tsbinarized.png"
        ]]
    shell:
        "python3 code/ts2plots.py --res_simu_fname {input.ts} --plots all --outplots_dirpath {params.outdir} --prefix_outfile {params.prefix} --globally {config[globalbinarizationthreshold]}"


rule pknsif2pkndot:
    input:
        config['folder_pkn'] / "{SYSTEM_id}.sif"
    output:
        config['folder_pkn'] / "{SYSTEM_id}.dot"
    shell:
        "python3 code/pknsif2pkndot.py --siffilepath {input} --dotfilepath {output}"


rule pkndot2pknsvg:
    input:
        config['folder_pkn'] / "{SYSTEM_id}.dot"
    output:
        config['folder_pkn'] / "{SYSTEM_id}.svg"
    shell:
        "dot -Tsvg {input} -o {output}"


rule breakpknsvg:
    input:
        config['folder_pkn'] / "{SYSTEM_id}.svg"
    output:
        dynamic(config['folder_pkn'] / "{SYSTEM_id}_{node}.svg")
    shell:
        "python3 code/svgcompletepkn2svgpknpernode.py  --svgfile {input}"


rule stgsif2stgpng:
    input:
        config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" / \
            "{BN_id}_stg-{updatescheme}_{focus}.sif"
    output:
        config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
        "{BN_id}_stg-{updatescheme}_{focus}.png"
    shell:
        "python3 code/stgsif2stgpng.py --stgsif {input} --colorobsconfigs --drawobsseq --outpng {output}"


config["folder_bn"] / "{SYSTEM_id}" / \
    "{identification_method}" / \
    "{BN_id}_coverage_stgVSts_{coverage_updatescheme}.csv"


rule get_coverageboxplot:
    input:
        lambda wildcards: [str(p) for p in (config["folder_bn"] / f"{wildcards.SYSTEM_id}"
                                            / f"{wildcards.identification_method}").glob(f"*_coverage_stgVSts_{wildcards.coverage_updatescheme}.csv")]
        # expand(
        #     config["folder_bn"] / "{{SYSTEM_id}}" /
        #     "{{identification_method}}" /
        #     "{BN_id}_coverage_stgVSts.csv",
        #     BN_id=glob_wildcards(config["folder_bn"] / "{{SYSTEM_id}}" /
        #                          "{{identification_method}}" /
        #                          "{BN_id}_coverage_stgVSts.csv")[0]
        # )
    params:
        measure = lambda w: w.measure
    output:
        config["folder_bn"] / "{SYSTEM_id}" / \
            "{identification_method}" / \
            "coverage-{SYSTEM_id}-{identification_method}-{measure}-{coverage_updatescheme}-boxplot_stgVSts.png"
    shell:
        "python3 code/coverageboxplot.py --boxplotfilepath {output} --identification_method {wildcards.identification_method} --boxplottitle 'Coverage ({wildcards.coverage_updatescheme} {wildcards.measure})\nfor {wildcards.SYSTEM_id}' --measure {params.measure} --updatescheme {wildcards.coverage_updatescheme} --coveragefilepaths {input}"
