
# === Get data ===
# --- All in one go :
snakemake -s OLA2021.smk all_setupdata --cores 4 --config dir_xp="experiments/xpname" --reason -p --configfile xpconfig_OLA21.yml  --notemp --keep-going


# === BNs synthesis ===
# --- Run all the methods on all the systems :
snakemake -s OLA2021.smk all_BNsidentification --cores 4 --config dir_xp="experiments/xpname" --reason -p --configfile xpconfig_OLA21.yml  --notemp --keep-going


# === BNs analysis ===
# with batch :
# better in cases there are a lot of bnet to process.
# the batch can only be performed on aggregations rules, which in this case is all_get_coverage_stgVSts
# https://github.com/snakemake/snakemake/issues/98
for i in {1..10}
do
  snakemake -s OLA2021.smk all_get_coverage_stgVSts --cores 5 --config dir_xp="experiments/xpname" --reason -p --configfile xpconfig_OLA21.yml  --notemp -k --batch all_get_coverage_stgVSts=$i/10
done

# --- Get comparative coverage boxplot
# comparative boxplots for OLA21 submission.
# one figure per system, one boxplot per identification method (for a given coverage measure and stg update scheme)
snakemake -s OLA2021.smk all_figure_comparisoncoverage --cores 1 --config dir_xp="experiments/xpname" -p --configfile xpconfig_OLA21.yml  --notemp --forcerun figure_comparisoncoverage
snakemake -s OLA2021.smk all_figure_comparisoncoverage_PyBoolNetrepo --cores 1 --config dir_xp="experiments/xpname" -p --configfile xpconfig_OLA21.yml  --notemp --forcerun figure_comparisoncoverage


# --- Get table number of BNs generated for all systems by all the identification methods
snakemake -s OLA2021.smk all_number_generatedBNs --cores 1 --config dir_xp="experiments/xpname" -p --configfile xpconfig_OLA21.yml
snakemake -s OLA2021.smk all_aggregationresults --cores 1 --config dir_xp="experiments/xpname" -p --configfile xpconfig_OLA21.yml
snakemake -s OLA2021.smk all_aggregation_results_image --cores 1 --config dir_xp="experiments/xpname" -p --configfile xpconfig_OLA21.yml


#--- Figures Running example :
snakemake -s OLA2021.smk figpaper_all --cores 1 --config dir_xp="experiments/xpname" -p --configfile xpconfig_OLA21.yml
